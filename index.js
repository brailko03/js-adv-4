"use strict";

// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX це технологія, що дозволяє здійснювати асинхронний обмін даними між 
// клієнтом та сервером без потреби перезавантажувати сторінку.
// Завдяки AJAX можна оновлювати окремі частини сторінки  на клієнті.



// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, 
// а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.


fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(res => res.json())
    .then(data => {
        console.log(data);
        data.forEach(({ episodeId, name, openingCrawl, characters }) => {
            const card = document.createElement("div");
            card.className = "card"

            const filmId = document.createElement("span");
            const filmName = document.createElement("h1");
            const summary = document.createElement("p");
            const list = document.createElement("ul");

            filmId.innerText = `Episode ${episodeId}`;
            filmName.innerText = `${name}`;
            summary.innerText = `${openingCrawl}`;
            list.innerText = "Characters:"

            card.append(filmId, filmName, summary, list);
            document.body.append(card);



            characters.forEach(character => {
                fetch(character)
                    .then(res => res.json())
                    .then(({ name }) => {
                        const li = document.createElement("li");
                        li.innerText = `${name}`;
                        card.append(li)
                    })
            })

        })

    })

